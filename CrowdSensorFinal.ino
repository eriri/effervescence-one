/***************************************************
 * Crowd Monitoring Sensor Code developed for PICO in 2019.
 * Credits to Adafruit for providing their libraries and example codes for uploading of data values to the Adafruit.io server
 * 
 * Title of example code used:
  Adafruit MQTT Library ESP8266 Example

  Must use ESP8266 Arduino from:
    https://github.com/esp8266/Arduino

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Tony DiCola for Adafruit Industries.
  MIT license, all text above must be included in any redistribution

 ****************************************************/
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <NewPingESP8266.h>

int server_number = 4;                  // Server Numbers: 1: S1,S2 2: S3,S4 3: S5,S6 4: S7,S8 5: S9,S10 ... Choose server for its respective sensor to be programmed.


/************************* WiFi Access Point *********************************/

const String WLAN_SSID[3]       {"Connectify-me","WLAN_2","WLAN_3"};                                                  // List of Usable WiFis (Add More Accordingly)
                      
const String WLAN_PASS[3]       {"Pico2019","PASS_1","PASS_2"};                                                   // List of Usable Wifi Passwords  (Add more Accordingly)

int WIFI_NUMBER = 0;                                                                                                 // Choose Index that selects wifi name and password from list here

/************************* Adafruit.io Setup *********************************/

const char* AIO_KEY_LIB[]     {"d3feb983d7f143d5864f81116930f2e6","c54861cf59ae4d0fad91dc8c1c440b05","3c4e6b59b2e04f97aa8d3cfc3d3b182e","f4a490b3926c4d90be4873894ea35f22","861ce1f0f92b430ca6fe646819087a71"};     // Adafruit IO Keys for different servers

#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                                // use 8883 for SSL
#define AIO_USERNAME    "Effervescence4"                    // Change this for each respective server

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY_LIB[server_number-1]);

/****************************** Feeds ***************************************/

//Sonar Sensor Variables
// SONAR pins numbers

#define trigPin 12
#define echoPin 13
#define trigPin2 16
#define echoPin2 14
#define MAX_DISTANCE 500

// LED pins

#define ledPin1 4
#define ledPin2 5

//Using New NewPing Library

NewPingESP8266 sonar1(trigPin, echoPin, MAX_DISTANCE);
NewPingESP8266 sonar2(trigPin2, echoPin2, MAX_DISTANCE);

// Define Variables for use in WiFi

unsigned long lastSend;
unsigned long lastTrigger;    // For checking how long it has passed before uploading data
int counter;                  // Value parsed into server
int lastCount;

int triggerDistance = 200;     // Change this value for calibration (Everytime sensor is placed in a new place, change the value)

int WiFitimer;
int i = 0;

// Define Variables for use in Sonar

unsigned long sonartimeout;
int pulse1;
int pulse2;
int count1 = 0;
int count2 = 0;

// Setup a feed called 'area1' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>

Adafruit_MQTT_Publish area_publish = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/area7");                                                           // For uploading data

Adafruit_MQTT_Subscribe area_subscribe = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/area7");                                                     // For subscribing to server to retrieve last uploaded data

Adafruit_MQTT_Publish area_get = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/area7/get");                                                           // For sending a get request to retrieve last uploaded data


/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void setup() {

  pinMode(LED_BUILTIN, OUTPUT);                                     //Setup LED for notifying if Arduino is connected to server
  digitalWrite(LED_BUILTIN,LOW);

  pinMode(ledPin1,OUTPUT);                                          // ledPins 1 & 2
  digitalWrite(ledPin1,LOW);

  pinMode(ledPin2,OUTPUT);
  digitalWrite(ledPin2,LOW);
  
  Serial.begin(115200);
  delay(10);

  Serial.println(F("Adafruit MQTT demo"));

  // Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID[WIFI_NUMBER]);

  WiFi.begin(WLAN_SSID[WIFI_NUMBER], WLAN_PASS[WIFI_NUMBER]);
  WiFitimer = millis();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
//    if (WiFitimer > 5000 && i < 3){
//      i++;
//      Serial.println(); Serial.println();
//      Serial.print("Connecting to ");
//      Serial.println(WLAN_SSID[0]);
//      WiFi.begin(WLAN_SSID[i],WLAN_PASS[i]);
//      WiFitimer = 0;
//    }
    if (millis()>10000){
      abort();
    }
    
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&area_subscribe);

  digitalWrite(ledPin1,HIGH);
  digitalWrite(ledPin2,HIGH);

  delay(1000);
  
  digitalWrite(ledPin1,LOW);
  digitalWrite(ledPin2,LOW);

  /**********************************************Modified Section******************************************************/

  MQTT_connect();

  mqttGet(); 
                                                                                           // Get Request, Modified Sect
  delay(200);
                                                                                           
  Adafruit_MQTT_Subscribe *subscription;
     
  while ((subscription = mqtt.readSubscription(100))) {
//    mqttGet();                                                                                          // Get Request, Modified Sect
    if (subscription == &area_subscribe) {
      counter = atoi((char *)area_subscribe.lastread);
      Serial.print("Last Read Counter Value: ");
      Serial.println(counter);
    }
  }

  /*******************************************End of Modified Section*************************************************/
}

uint32_t x=0;

void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  
  MQTT_connect();

  // this is our 'wait for incoming subscription packets' busy subloop

/*********************************************Modified Section (2)****************************************************/

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(10))) {                                                                                          // Get Request, Modified Sect
    if (subscription == &area_subscribe) {
      counter = atoi((char *)area_subscribe.lastread);
      Serial.print("Counted Value Changed to: ");
      Serial.println(counter);
    }
  }
  
/********************************************End of Modified Section (2)***********************************************/

  updateCounter();                                                                                                   // Update Local Count Function triggered by people entering/exiting

  if ( counter - lastCount == 0 ) {                                                                                 // Check if current count and lastCount is the same   
    if ( millis() - lastTrigger > 5000) {                                                                             // If switch is already primed and 5 seconds has passed with it primed, update server
        mqttPublish();
        lastTrigger = millis();
    }   
  }
  
  else  {
    lastTrigger = millis();
    Serial.print("Counter changed to: ");
    Serial.println(counter);
  }

  lastCount = counter;      // Copy value of counter into lastCount

}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
    digitalWrite(LED_BUILTIN, HIGH);    // Check that Module is working without serial monitor
  }

  digitalWrite(LED_BUILTIN, LOW);     // Off LED if MQTT needs to reconnect
  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

/*************************** Publish to Adafruit IO ************************************/

void mqttPublish()  {

  // Now we can publish stuff!
  Serial.print(F("\nSending Counter val "));
  Serial.print(counter);
  Serial.print("...");
  if (! area_publish.publish(counter)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  /*
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  */

}

void mqttGet()  {

  // Similar to publish, but for getting most recent data
  Serial.print(F("\nSending Get Request!"));
  if (! area_get.publish("\0")) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }
}

/*************************** Sonar Local Update Code ************************************/

void updateCounter() {                          
  
  pulse1 = sonar1.ping_cm();
  if (pulse1==0){
    pulse1 = MAX_DISTANCE;
  }

  delay(10);

  pulse2 = sonar2.ping_cm();
  if (pulse2==0){
    pulse2 = MAX_DISTANCE;
  }

  delay(10);

  
  Serial.print("pulse1: ");     //CHECK SONAR VALUES
  Serial.print(pulse1);
  Serial.print(" pulse2: ");
  Serial.print(pulse2);
  Serial.print(" Count 1: ");
  Serial.print(count1);
  Serial.print(" Count 2: ");
  Serial.println(count2);

  if (count1 == 0 && count2 ==0){
    if (pulse1 < triggerDistance){
      count1 = 1;
      sonartimeout = millis();
    }
    else if (pulse2 < triggerDistance){
      count2 = 1;
      sonartimeout = millis();
    }
  }

  else if (count1 == 1){
    if (pulse2 < triggerDistance && millis()- sonartimeout < 500){
      counter += 1;
      count1 = 0;
      digitalWrite(ledPin2,HIGH);
      delay(1500);
      digitalWrite(ledPin2,LOW);
    }

    else if (millis()-sonartimeout > 500){
      count1 = 0;
    }
  }

  else if (count2 == 1){
    if (pulse1 < triggerDistance && millis() - sonartimeout < 500){
      counter -= 1;
      count2 = 0;
      digitalWrite(ledPin1,HIGH);
      delay(1500);
      digitalWrite(ledPin1,LOW);
    }

    else if (millis()-sonartimeout > 500){
      count2 = 0;
    }
  } 
}
