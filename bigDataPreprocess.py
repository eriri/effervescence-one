import pandas as pd
import glob
import os
from functools import reduce

def make_df(inFiles):
	dfs = []
	for file in inFiles:
		df = pd.read_csv(file, skiprows = 2)
		dfs.append(df)
	merged_df = reduce(lambda left, right: pd.merge(left, right, on="Time"), dfs)
	return merged_df

def calculate(merged_df_pos, merged_df_neg, flag):
	merged_df_pos = merged_df_pos.drop(["Start", "End"], axis=1) 
	merged_df_neg = merged_df_neg.drop(["Start", "End"], axis=1)
	list_mood = []
	for row in range (0, len(merged_df_pos)):
		pos = 0
		total = 0
		for col in range (1, 6):
			total += merged_df_pos.iloc[row, col] + merged_df_neg.iloc[row, col]
			pos += merged_df_pos.iloc[row, col]
		mood = pos / total
		if (mood != 0.0 and mood != 1.0):
			list_mood.append(mood)
	print(flag, "min", min(list_mood))
	print(flag, "max", max(list_mood))
	list_norm = []
	for i in list_mood:
		norm_mood = (i - min(list_mood)) / (max(list_mood) - min(list_mood))
		list_norm.append(norm_mood)
	all_mood = {flag : list_norm}
	out_df = pd.DataFrame(all_mood)
	return out_df

def merge_df(allModes_df, nonfic_df, fantasy_df, romance_df, scifi_df, outFile):
	allModes_df = allModes_df.drop(["Start", "End"], axis=1)
	merged_df = pd.concat([allModes_df, nonfic_df, fantasy_df, romance_df, scifi_df], axis=1)
	merged_df.to_csv(outFile, index=False, encoding="utf-8-sig")

if __name__ == "__main__":
	#TODO: CHANGE ROOT FOLDER PATH WITH ALL GOOGLE TRENDS FILES HERE
	root = "/Users/admin/Documents/GoogleTrends"

	scifi_pos_in = glob.glob(os.path.join(root, "Scifi_Pos") + "/*.csv")
	scifi_neg_in = glob.glob(os.path.join(root, "Scifi_Neg") + "/*.csv")
	scifi_df = calculate(make_df(scifi_pos_in), make_df(scifi_neg_in), "Mood_Science Fiction")

	fantasy_pos_in = glob.glob(os.path.join(root, "Fantasy_Pos") + "/*.csv")
	fantasy_neg_in = glob.glob(os.path.join(root, "Fantasy_Neg") + "/*.csv")
	fantasy_df = calculate(make_df(fantasy_pos_in), make_df(fantasy_neg_in), "Mood_Fantasy")

	nonfic_pos_in = glob.glob(os.path.join(root, "Nonfic_Pos") + "/*.csv")
	nonfic_neg_in = glob.glob(os.path.join(root, "Nonfic_Neg") + "/*.csv")
	nonfic_df = calculate(make_df(nonfic_pos_in), make_df(nonfic_neg_in), "Mood_Non-Fiction")

	romance_pos_in = glob.glob(os.path.join(root, "Romance_PG13") + "/*.csv")
	romance_neg_in = glob.glob(os.path.join(root, "Romance_R21") + "/*.csv")
	romance_df = calculate(make_df(romance_pos_in), make_df(romance_neg_in), "Mood_Romance")

	modes_in = glob.glob(os.path.join(root, "All_Modes") + "/*.csv")
	allModes_df = make_df(modes_in)

	outFile = os.path.join(root, "BigData.csv")
	merge_df(allModes_df, nonfic_df, fantasy_df, romance_df, scifi_df, outFile)
