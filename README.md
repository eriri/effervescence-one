# Post Exhibition Guide
Important information on how to edit this Unity project for future development.

## Colors

Colors can be adjusted through the **Controllers** game object in the scene. Under the **Mood Controller** script attached to the game object, there is a list of color palettes that sets the waterfall color for each genre, as shown in the image below. 

![Screenshot 0](color0.png)

## Big Data

Search interest data can be retrieved from [Google Trends](http://www.trends.google.com). Enter the desired search term(s) as shown in the figure below. A maximum of 5 search terms can be entered. However, for better results amongst uncommon search terms, we split the search to accommodate for only one search term per search. Set the desired time period, e.g. Past Hour and region, e.g. Worldwide, as shown in the figure below. Google Trends returns the search interest of each search term as a search popularity proportion of the total highest search interest. For example, a search interest of 100 for `Non-Fiction Books` indicates that `Non-Fiction Books` has the highest number of searches at that minute.

Retrieve the search interest data for each book genre, e.g. search interest for `Non-Fiction Books`, and search interest data for each characteristic search term, e.g. search interest for `Health Care`. Download the respective CSV file(s) by clicking on the icon, as shown in the figure below.

![Trend 0](trend0.png)

### Data Preprocessing

> Please ensure that the following packages/libraries are installed: *Python, Pandas, Glob and Functools*.

Place the CSV files for the mode data into a folder named `All_Modes`. Place the CSV files for the characteristic data with the same sentiments for each genre into the same folder. Hence, there will be a total of *8* folders for *4* modes with *2* polar sentiments each. **Use bigDataPreprocess.py** script to preprocess the data.

Change the path of the root folder in the following line of code in the main class of **bigDataPreprocess.py**.

```python
if __name__ == “__main__”:
	# Change the root folder path with all Google Trends files here    
	root = “ /Users/admin/Documents/GoogleTrends ” 
```

The path of each mode-sentiment folder can be changed in the main class of **bigDataPreprocess.py**. For example, for `Non-Ficion` genre, the CSV filled with calm sentiments are placed into the folder named `Nonfic_Pos`, while those with turbulent sentiments are placed into the folder named `Nonfic_Neg`.

```python
nonfic_pos_in = glob.glob(os.path.join(root, “Nonfic_Pos”)  + “/*.csv”)
nonfic_neg_in = glob.glob(os.path.join(root, “Nonfic_Neg”)  + “/*.csv”)
```

As there may be a time difference between the time that the CSV files are being downloaded, to sync up the data, make a duplicate of the first file downloaded and the last file downloaded and rename the header of the second column of the first file to `Start` and that of the last file to `End` (see figure below).

![CSV 0](csv0.png)

After running **bigDataPreprocess.py**, a CSV file named `BigData.csv` will be produced and can now be passed into Unity.

### Using Big Data in Unity

Place the BigData.csv file in the Assets folder in Unity.

All 4 book genres were parsed into Unity as separate lists, adjust the lists accordingly in **Controller.cs**, based on the number of modes that you have. Each mode has a list of search interest values for book genre data and a list of search interest values for characteristic data, as shown in the following lines:

```c#
nonficGen.Add(int.Parse(lineValues[1]));
fantasyGen.Add(int.Parse(lineValues[2]));
romanceGen.Add(int.Parse(lineValues[3]));
scifiGen.Add(int.Parse(lineValues[4]));

nonficMood.Add(int.Parse(lineValues[5]));
fantasyMood.Add(int.Parse(lineValues[6]));
romanceMood.Add(int.Parse(lineValues[7]));
scifiMood.Add(int.Parse(lineValues[8]));
```

Under the **Inspector** tab in Unity for **Controller** object,  tick the checkboxes in the **Controller (Script)** section for the modes to be enabled (see figure below).

![Controller 0](controller0.png)

The names of the checkboxes can be adjusted in Unity as shown in the following lines of code in the **Controller** script:

```c#
public bool nonFic;
public bool fantasy;
public bool romance;
public bool sciFi;
```

For example to change the name of the `Non Fic` checkbox to `Mode 1` checkbox, change `public bool nonFic` to `public bool mode1`.

### Adjusting Mode Timing

Under the **Inspector** tab of the **Controller** object, set the duration that the artwork stays within a certain mode in the **Controller (Script)** section (see figure below):

![Time 0](time0.png)

Alternatively, the duration can be set in the **Controller** script at `public float genreTime`. 

We have also calibrated the transition durations within and between each mode. 

To change the transition duration for entering a mode, adjust `private float genTransitionInTime = 8.0f;`

To change the transition duration for exiting a mode, adjust `private float genTransitionOutTime = 8.0f;`

To change the transition duration between characterizations (within each mode), adjust `private float moodTransitionTime = 5.0f;`

## Crowd Sensor Data

Crowd data is currently sent to Unity by querying the Adafruit servers with the AIO Key for their respective servers:

```c#
Dictionary<string, string> serverAddr = new Dictionary<string, string> 

{

{"sensor1", "https://io.adafruit.com/api/v2/Effervescence1/feeds/area1/data/last?X-AIO-Key=d3feb983d7f143d5864f81116930f2e6" },

{"sensor2", "https://io.adafruit.com/api/v2/Effervescence1/feeds/area2/data/last?X-AIO-Key=d3feb983d7f143d5864f81116930f2e6" },

{"sensor3", "https://io.adafruit.com/api/v2/Effervescence2/feeds/area3/data/last?X-AIO-Key=c54861cf59ae4d0fad91dc8c1c440b05" },

{"sensor4", "https://io.adafruit.com/api/v2/Effervescence2/feeds/area4/data/last?X-AIO-Key=c54861cf59ae4d0fad91dc8c1c440b05" },

{"sensor5", "https://io.adafruit.com/api/v2/Effervescence3/feeds/area5/data/last?X-AIO-Key=3c4e6b59b2e04f97aa8d3cfc3d3b182e" },

{"sensor6", "https://io.adafruit.com/api/v2/Effervescence3/feeds/area6/data/last?X-AIO-Key=3c4e6b59b2e04f97aa8d3cfc3d3b182e" },

{"sensor7", "https://io.adafruit.com/api/v2/Effervescence4/feeds/area7/data/last?X-AIO-Key=f4a490b3926c4d90be4873894ea35f22" },

{"sensor8", "https://io.adafruit.com/api/v2/Effervescence4/feeds/area8/data/last?X-AIO-Key=f4a490b3926c4d90be4873894ea35f22" },

{"sensor9", "https://io.adafruit.com/api/v2/Effervescence5/feeds/area9/data/last?X-AIO-Key=861ce1f0f92b430ca6fe646819087a71" },

{"sensor10", "https://io.adafruit.com/api/v2/Effervescence5/feeds/area10/data/last?X-AIO-Key=861ce1f0f92b430ca6fe646819087a71" }

};
```

### Arduino Setup

To view the crowd sensor data in the servers, go to https://io.adafruit.com/

Sensors 1 and 2 are connected to Server 1:

Username: `Effervescence1`

Password: `Pico2019`

Sensors 3 and 4 are connected to Server 2:

Username: `Effervescence2`

Password: `Pico2019`

Sensors 5 and 6 are connected to Server 3:

Username: `Effervescence3`

Password: `Pico2019`

Sensors 7 and 8 are connected to Server 4:

Username: `Effervescence4`

Password: `Pico2019`

Sensors 9 and 10 are connected to Server 5:

Username: `Effervescence5`

Password: `Pico2019`

Under Feeds, data for each sensor can be viewed.

1. Download Arduino IDE from https://www.arduino.cc/en/main/software.
2. Open Arduino IDE and go to *File >> Preferences*.
3. Copy and paste the following: [http://arduino.esp8266.com/stable/package_esp8266com_index.json](http://arduino.esp8266.com/stable/package_esp8266com_index.json) into the additional board manager URL text box in the dialog box that appears. 
4. Click ok to download the packages.
5. Go to *Tools >> Board >> Board Manager* in Arduino IDE. The Board Manager window will appear. 
6. Scroll down the boards in the board manager and select EPS8266 from the list of available boards.
7. Go to *Tools >> Board: “...”* and select WeMos D1R1 from the section.
8. Plug a micro USB cable from your computer to the sensor module and you are now able to upload the code Crowd Sensor Final into the Arduino. Ensure that the correct port is chosen when this is done. This is done by going to *Tools >> Port* and selecting the available port the Arduino is connected to. 

### Changing Parameters

1. Under comment `Wifi Access Point`, you will see `WLAN_SSID[3]` and `WLAN_PASS[3]`. These are lists of wlan points and their passwords which you can input your available access points and their passwords into. Just replace `WLAN_2` and `PASS_1` respectively to insert a new wifi and password. To select the access point and password to be used, change `int WIFI_number = 0;` to `int WIFI_number = [number];` 0 represents the 1st point in the list, 1 the 2nd, and so on.

2. Under Adafruit.io setup, 5 server keys have been placed. the list can be extended for more servers by just adding on to the keys. If no additions have been done, only one parameter needs to be changed in this section: `AIO_USERNAME`. `Effervescence1` is for sensors 1 and 2, `Effervescence2` is for sensors 3 and 4, and so on. Each server is responsible for holding the data of two sensors. Change `Effervescence[number]` respectively for the sensor which the program is going to be loaded into. 

3. Under Sonar Sensor Variables, change `int triggerDistance = ???;` for the distance at which the sensor is supposed to be triggered. Each sensor is supposed to be calibrated for different areas which they are used for. Distance is in *cm*.

4. For these three variables:

   ```c++
   Adafruit_MQTT_Publish area_publish = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/area?"); // For uploading data
   
   
   Adafruit_MQTT_Subscribe area_subscribe = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/area?");  // For subscribing to server to retrieve last uploaded data
   
   
   Adafruit_MQTT_Publish area_get = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/area?/get");
   ```

   Change the `area?` for each respective sensor. `area1` for sensor 1, `area2` for sensor 2 and so on. The rest of the program does not need to be altered in any manner, unless sensor algorithm needs to be fine-tuned.

### Perlin Mesh Height

Perlin mesh height is set according to the number of people detected by the crowd sensors. However, for better visual effects, we set maximum and minimum height to the mesh in `numCap` and `heightMin` in the **Mesh Animator** script attached to the **MeshAnimator** game object. In addition, there is a series of multipliers that scales the increase in height according to the number of people detected in the same script so that the increase in height is made more obvious when there is a low number of people. Below is the relevant code snippet from the script. 

```c#
// Loop through each sensor
for (int k = 0; k < CDList.Count; k++)
{
    // Gives mesh minimum height if no one has been detected
    if (CDList[k] < 1)
    {
        capCDList.Add(heightMin);
    }
    // Set height as rescaleL1 if there's one person
    else if (CDList[k] == 1)
    {
        capCDList.Add(CDList[k] * rescaleL1);
    }
    // Set height as 2 * rescaleL2 if there are two people
    else if (CDList[k] == 2)
    {
        capCDList.Add(CDList[k] * rescaleL2);
    }

    else if (CDList[k] == 3)
    {
        capCDList.Add(CDList[k] * rescaleL3);
    }

    else if (CDList[k] < 6)
    {
        capCDList.Add(CDList[k] * rescale);
    }
    else {
        // Set height as numCap * rescaleH if number of 
        // people exceeds five           
        capCDList.Add(numCap * rescaleH); }
}
```

## Kinect

To match the actual position of users with the position of interactions on the wide library screen used during the exhibition, we scaled the position of the detected joints in the x and z axis. They can be adjusted through the **Body Source View** script attached to the **BodyView** object in line 380, as show below.

```c#
private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
{
    return new Vector3(
        joint.Position.X * 2.0f, // Scales x 
        joint.Position.Y * 1f, // Scales y
        joint.Position.Z * 0.8f); // Scales z
}
```
## VFX Graph

Some parameters, such as stick forces and attraction speeds, for the VFX graph particle system can be adjusted through the Unity Editor under the **Visual Effect** game object, as shown in the image below. `stick force s` refers to the stick force for Kinect interaction for sci-fi genre. Adjust `face strecher ` and `mesh strecher` to scale size of the waterfall and mesh according to screen resolution. 

![Screenshot 3](color3.png)

However, **turbulence** and **gravity** (external force) can only be adjusted through the **Mood Controller** script. For example, for sci-fi genre, we define a maximum and minimum turbulence value each for both large and small particles, and interpolate between the extremes according to the sentiment value, as shown below. 

```c#
// Turbulence for large particles when sentiment = 1
// turbulenceSpL = 'turbulence Sci-fi positive large'
private float turbulenceSpL = 0.01f; 
// turbulenceSpL = 'turbulence Sci-fi positive small'
private float turbulenceSpS = 0f;   

...
    
// turbulenceSpL = 'turbulence Sci-fi negative large'
private float turbulenceSnL = 0.5f;

// turbulenceSpL = 'turbulence Sci-fi negative small'
private float turbulenceSnS = 0.4f;

...

// Interpolate based on mood (sentiment) value
ve.SetFloat("intensity turbulence", turbulenceSpS * mood + turbulenceSnS * (1 - mood));

ve.SetFloat("turbulence secondary", turbulenceSpL * mood + turbulenceSnL * (1 - mood));
```
## Sound

Background music for each genre can be adjusted through the **Controllers** game object in the scene, under the **Controller** script attached to the game object. Please refer to the image below.

![Screenshot 1](color1.png)

Under `clips`, `Element 0` corresponds to the BGM for non-fiction, `Element 1` to fantasy, `Element 2` to romance and `Element 3` to sci-fi.

Sound effects for Kinect interaction can be adjusted under `Clip` in the **Body Source View** script attached to the **BodyView**, as shown below.

![Screenshot 2](color2.png)