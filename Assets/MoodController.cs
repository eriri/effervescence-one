﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.VFX;

public class MoodController : MonoBehaviour
{
    // Perlin mesh
    // Controls y coordinate of the Perlin mesh ("rocky") that responds to crowd data inputs
    private float meshY = -0.5f;
    //Controls Perlin mesh scale
    private Vector3 meshScale = new Vector3(16f, 5f, -10f);

    // Spawn map
    // Controls spawn map scale
    private Vector3 spawnMapScale = new Vector3(3f, 0.035f, 0.4f);

    // Colors used, 4 keys each for alpha & color by default 
    // Need to be set through editor as HDR intensity cannot be accessed directly
    private Gradient gradient;
    private Gradient gradientSec;

    // Buffer for transition between genres
    private Gradient gradientOld;
    private Gradient gradientSecOld;

    private float turbulenceL; // L represents large & textured particles
    private float turbulenceS; // S represents small particles that outline the Perlin mesh

    private float turbulenceLOld;
    private float turbulenceSOld;

    private Vector3 forceL;
    private Vector3 forceS;

    private Vector3 forceLOld;
    private Vector3 forceSOld;

    // Genres
    private enum Genre { NonFi, Fantasy, Romance, SciFi };
    private Genre c;

    // Fantasy ------------------------------------------------------------------------------------------
    private float turbulenceFp = 0.08f;
    private float turbulenceFn = 0.18f;

    private Vector3 forceFp = new Vector3(0, -0.03f, 0);
    private Vector3 forceFn = new Vector3(-0.01f, -0.035f, 0.01f);

    //private float attractionSpeedF = 4f; // kinect parameters do not change with mood
    //private float attractionForceF = 10f;
    //private float stickForceF = 1f;
    //private float stickDistanceF = 0.3f;

    // ++
    [Header("Fantasy (+ve)")]
    [ColorUsage(true, true)]
    public Color colorFp0 = new Color();
    [ColorUsage(true, true)]
    public Color colorFp1 = new Color();
    [ColorUsage(true, true)]
    public Color colorFp2 = new Color();
    [ColorUsage(true, true)]
    public Color colorFp3 = new Color();

    // --
    [Header("Fantasy (-ve)")]
    [ColorUsage(true, true)]
    public Color colorFn0 = new Color();
    [ColorUsage(true, true)]
    public Color colorFn1 = new Color();
    [ColorUsage(true, true)]
    public Color colorFn2 = new Color();
    [ColorUsage(true, true)]
    public Color colorFn3 = new Color();

    // Romance ------------------------------------------------------------------------------------------
    private float turbulenceRpL = 0.1f;     
    private float turbulenceRpS = 0.2f;

    private Vector3 forceRpL = new Vector3(0, -0.015f, 0);
    private Vector3 forceRpS = new Vector3(0, -0.02f, 0);

    private float turbulenceRnL = 0.15f;
    private float turbulenceRnS = 0.2f;

    private Vector3 forceRnL = new Vector3(0, -0.02f, 0.01f);
    private Vector3 forceRnS = new Vector3(0, -0.01f, 0);

    //private float attractionSpeedR = 3.5f; // kinect parameters do not change with mood
    //private float attractionForceR = 7f;
    //private float stickForceR = 6f;
    //private float stickDistanceR = 0.3f;

    // ++
    [Header("Romance small (+ve)")]
    [ColorUsage(true, true)]
    public Color colorRpS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpS3 = new Color();

    [Header("Romance large (+ve)")]
    [ColorUsage(true, true)]
    public Color colorRpL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorRpL3 = new Color();

    // --
    [Header("Romance small (-ve)")]
    [ColorUsage(true, true)]
    public Color colorRnS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnS3 = new Color();

    [Header("Romance large (-ve)")]
    [ColorUsage(true, true)]
    public Color colorRnL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorRnL3 = new Color();

    // Sci-fi ------------------------------------------------------------------------------------------
    private float turbulenceSpL = 0.01f;     
    private float turbulenceSpS = 0f;     

    private Vector3 forceSpL = new Vector3(0, -0.05f, 0);
    private Vector3 forceSpS = new Vector3(0, -0.03f, 0);

    private float turbulenceSnL = 0.5f;
    private float turbulenceSnS = 0.4f;

    private Vector3 forceSnL = new Vector3(0, -0.08f, 0);
    private Vector3 forceSnS = new Vector3(0, -0.06f, 0.01f);

    //private float attractionSpeedS = 4f; // kinect parameters do not change with mood
    //private float attractionForceS = 10f;
    //private float stickForceS = 6f;
    //private float stickDistanceS = 0.2f;

    // ++
    [Header("Science-Fiction small (+ve)")]
    [ColorUsage(true, true)]
    public Color colorSpS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpS3 = new Color();

    [Header("Science-Fiction large (+ve)")]
    [ColorUsage(true, true)]
    public Color colorSpL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorSpL3 = new Color();

    // --
    [Header("Science-Fiction small (-ve)")]
    [ColorUsage(true, true)]
    public Color colorSnS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnS3 = new Color();

    [Header("Science-Fiction large (-ve)")]
    [ColorUsage(true, true)]
    public Color colorSnL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorSnL3 = new Color();

    // Non-fiction ------------------------------------------------------------------------------------------
    private float turbulenceNpL = 0.2f;     
    private float turbulenceNpS = 0.2f;    

    private Vector3 forceNpL = new Vector3(0, -0.015f, 0);
    private Vector3 forceNpS = new Vector3(0, -0.04f, 0);

    private float turbulenceNnL = 0.9f;
    private float turbulenceNnS = 0.9f; 

    private Vector3 forceNnL = new Vector3(0, -0.025f, 0f);
    private Vector3 forceNnS = new Vector3(0, -0.05f, 0);

    //private float colliderSizeN = 4f; // size of collider for Kinect interactions

    // ++
    [Header("Non-Fiction small (+ve)")]
    [ColorUsage(true, true)]
    public Color colorNpS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpS3 = new Color();

    [Header("Non-Fiction large (+ve)")]
    [ColorUsage(true, true)]
    public Color colorNpL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorNpL3 = new Color();

    // --
    [Header("Non-Fiction small (-ve)")]
    [ColorUsage(true, true)]
    public Color colorNnS0 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnS1 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnS2 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnS3 = new Color();

    [Header("Non-Fiction large (-ve)")]
    [ColorUsage(true, true)]
    public Color colorNnL0 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnL1 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnL2 = new Color();
    [ColorUsage(true, true)]
    public Color colorNnL3 = new Color();

    // Gradient key timings
    [Header("Gradient key timming")]
    public float time0 = 0f;
    public float time1 = 0.37f;
    public float time2 = 0.72f;
    public float time3 = 1f;

    [Header("Mood control")]
    [Range(0f, 1f)]
    public float mood = 1f;
    //public Slider moodSlider;

    // VFX component
    public VisualEffect ve;

    // Start is called before the first frame update
    void Start()
    {
        mood = 1;

    }

    // Update is called once per frame
    void Update()
    {

        ve.SetFloat("mesh y", meshY);

        mood = Controller.mood; //moodSlider.value;
        c = (Genre) Controller.index;

        switch (c)
        {
            case Genre.Fantasy:
                ve.SetFloat("intensity turbulence", turbulenceFp * mood + turbulenceFn * (1 - mood));
                ve.SetVector3("gravity f", forceFp * mood + forceFn * (1 - mood));

                gradient = gradient_test(colorFp0, colorFn0, colorFp1, colorFn1, colorFp2, colorFn2, colorFp3, colorFn3);
                ve.SetGradient("color gradient", gradient);
                break;
            case Genre.Romance:
                ve.SetFloat("turbulence r2", turbulenceRpS * mood + turbulenceRnS * (1 - mood));
                ve.SetFloat("turbulence r1", turbulenceRpL * mood + turbulenceRnL * (1 - mood));

                ve.SetVector3("gravity r2", forceRpS * mood + forceRnS * (1 - mood));
                ve.SetVector3("gravity r1", forceRpL * mood + forceRnL * (1 - mood));

                gradient = gradient_test(colorRpS0, colorRnS0, colorRpS1, colorRnS1, colorRpS2, colorRnS2, colorRpS3, colorRnS3);
                ve.SetGradient("color gradient r2", gradient);

                gradientSec = gradient_test(colorRpL0, colorRnL0, colorRpL1, colorRnL1, colorRpL2, colorRnL2, colorRpL3, colorRnL3);
                ve.SetGradient("color gradient r1", gradientSec);
                break;
            case Genre.SciFi:
                ve.SetFloat("intensity turbulence", turbulenceSpS * mood + turbulenceSnS * (1 - mood));
                ve.SetFloat("turbulence secondary", turbulenceSpL * mood + turbulenceSnL * (1 - mood));

                ve.SetVector3("external force", forceSpS * mood + forceSnS * (1 - mood));
                ve.SetVector3("force secondary", forceSpL * mood + forceSnL * (1 - mood));

                gradient = gradient_test(colorSpS0, colorSnS0, colorSpS1, colorSnS1, colorSpS2, colorSnS2, colorSpS3, colorSnS3);
                ve.SetGradient("color gradient s2", gradient);

                gradientSec = gradient_test(colorSpL0, colorSnL0, colorSpL1, colorSnL1, colorSpL2, colorSnL2, colorSpL3, colorSnL3);
                ve.SetGradient("color gradient s1", gradientSec);
                break;
            case Genre.NonFi:
                ve.SetFloat("turbulence n2", turbulenceNpS * mood + turbulenceNnS * (1 - mood));
                ve.SetFloat("turbulence n1", turbulenceNpL * mood + turbulenceNnL * (1 - mood));

                ve.SetVector3("gravity n2", forceNpS * mood + forceNnS * (1 - mood));
                ve.SetVector3("gravity n1", forceNpL * mood + forceNnL * (1 - mood));

                gradient = gradient_test(colorNpS0, colorNnS0, colorNpS1, colorNnS1, colorNpS2, colorNnS2, colorNpS3, colorNnS3);
                ve.SetGradient("color gradient n2", gradient);

                gradientSec = gradient_test(colorNpL0, colorNnL0, colorNpL1, colorNnL1, colorNpL2, colorNnL2, colorNpL3, colorNnL3);
                ve.SetGradient("color gradient n1", gradientSec);
                break;
            default:
                break;
        }

       

    }

    Gradient gradient_test(Color p0, Color n0, Color p1, Color n1, Color p2, Color n2, Color p3, Color n3)
    {
        Gradient gradient = new Gradient();

        int keys = 4;
        GradientColorKey[] colorKey = new GradientColorKey[keys];
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[keys];

        colorKey[0].color = mood * p0 + (1 - mood) * n0;
        colorKey[0].time = time0;

        colorKey[1].color = mood * p1 + (1 - mood) * n1;
        colorKey[1].time = time1;

        colorKey[2].color = mood * p2 + (1 - mood) * n2;
        colorKey[2].time = time2;

        colorKey[3].color = mood * p3 + (1 - mood) * n3;
        colorKey[3].time = time3;

        // Populate the alpha keys
        alphaKey[0].alpha = mood * p0.a + (1 - mood) * n0.a;
        alphaKey[0].time = time0;

        alphaKey[1].alpha = mood * p1.a + (1 - mood) * n1.a;
        alphaKey[1].time = time1;

        alphaKey[2].alpha = mood * p2.a + (1 - mood) * n2.a;
        alphaKey[2].time = time2;

        alphaKey[3].alpha = mood * p3.a + (1 - mood) * n3.a;
        alphaKey[3].time = time3;

        gradient.SetKeys(colorKey, alphaKey);

        //Debug.Log(gradient.Evaluate(0.25f));
        return gradient;
    }
}
