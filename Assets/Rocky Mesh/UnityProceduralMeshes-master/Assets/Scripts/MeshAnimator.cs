﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class MeshAnimator : MonoBehaviour {

	private Material material;
	public int resolution = 64;
	public float noiseSpeed = 0.1f;
	public float noiseFrequency = 0.2f;
	public float noiseAmplitude = 0.1f;

	public Mesh planeMesh;

    private ImprovedPerlin perlin;
	private float currentNoiseOffset = 0f;
	private Vector3[] cacheVertices;

    // Maximum amplitude for Perlin mesh 
    private float numCap = 10.0f;
    // Minimum amplitude for Perlin mesh 
    private float heightMin = 0.5f;
    // Multiplier
    private float rescaleL1 = 1f;
    private float rescaleL2 = 0.6f;
    private float rescaleL3 = 0.4f;
    private float rescale = 0.35f;
    private float rescaleH = 0.348f;

    // Crowd stations
    float[] CDs;

    public float CD1;
    public float CD2;
    public float CD3;
    public float CD4;
    public float CD5;
    //public float CD6;
    //public float CD7;
    //public float CD8;
    //public float CD9;
    //public float CD10;

    // Lists to store the crowd stations
    List<float> CDList = new List<float>();
    List<float> capCDList = new List<float>();

    int numSensors = 5;

    // Use this for initialization
    void Start () {

        // Generate the mesh
        planeMesh = PlaneHelper.CreatePlane(resolution, resolution);
        transform.localScale = new Vector3(1, 1, 1);

		// Add this whenever you'll be updating the mesh frequently
		planeMesh.MarkDynamic();

		// Add these components for our GameObject to be able to display meshes
		MeshFilter mf = gameObject.AddComponent<MeshFilter>();
		//MeshRenderer mr = gameObject.AddComponent<MeshRenderer>();

		// Assign our new mesh to this gameobject
		// SharedMesh means the data will be a pointer instead of copied
		mf.sharedMesh = planeMesh;
		//mr.material = material;

		// Cache the vertex array so we won't need to allocate each frame
		cacheVertices = planeMesh.vertices;

		perlin = new ImprovedPerlin();
	}
	
	// Update is called once per frame
	void Update () {

        CDList.Clear();

        CD1 = API2.cdBind["area1"];
        CD2 = API2.cdBind["area2"];
        CD3 = API2.cdBind["area3"];
        CD4 = API2.cdBind["area4"];
        CD5 = API2.cdBind["area5"];

        CDs = new float[] { CD1, CD2, CD3, CD4, CD5};
        CDList.AddRange(CDs);
        capCDList.Clear();
       
        // Loop through each sensor
        for (int k = 0; k < CDList.Count; k++)
        {
            // Gives mesh minimum height if no one has been detected
            if (CDList[k] < 1)
            {
                capCDList.Add(heightMin);
            }
            // Set height as rescaleL1 if there's one person
            else if (CDList[k] == 1)
            {
                capCDList.Add(CDList[k] * rescaleL1);
            }
            // Set height as 2 * rescaleL2 if there are two people
            else if (CDList[k] == 2)
            {
                capCDList.Add(CDList[k] * rescaleL2);
            }

            else if (CDList[k] == 3)
            {
                capCDList.Add(CDList[k] * rescaleL3);
            }

            else if (CDList[k] < 6)
            {
                capCDList.Add(CDList[k] * rescale);
            }
            else {
                // Set height as numCap * rescaleH if number of people exceeds five           
                capCDList.Add(numCap * rescaleH); }
        }

        Debug.Log(String.Join("; ", CDList));
        Debug.Log(String.Join("; ", capCDList));

        float dist = cacheVertices[cacheVertices.Length - 1].x - cacheVertices[0].x;
        currentNoiseOffset += Time.deltaTime * noiseSpeed;

        int i = 0;
		for(int y = 0; y < resolution; y++){
			for(int x = 0; x < resolution; x++){

                if (cacheVertices[i].x <= cacheVertices[0].x + (dist / numSensors) )
                {
                    cacheVertices[i].y = perlin.Noise(x * noiseFrequency * capCDList[0], y * noiseFrequency * capCDList[0], currentNoiseOffset) * noiseAmplitude * capCDList[0];
                    i++;
                }
                else if (cacheVertices[i].x <= cacheVertices[0].x + (dist / numSensors) *2)
                {
                    cacheVertices[i].y = perlin.Noise(x * noiseFrequency * capCDList[1], y * noiseFrequency * capCDList[1], currentNoiseOffset) * noiseAmplitude * capCDList[1];
                    i++;
                }
                else if (cacheVertices[i].x <= cacheVertices[0].x + (dist / numSensors) *3)
                {
                    cacheVertices[i].y = perlin.Noise(x * noiseFrequency * capCDList[2], y * noiseFrequency * capCDList[2], currentNoiseOffset) * noiseAmplitude * capCDList[2];
                    i++;
                }
                else if (cacheVertices[i].x <= cacheVertices[0].x + (dist / numSensors) *4)
                {
                    cacheVertices[i].y = perlin.Noise(x * noiseFrequency * capCDList[3], y * noiseFrequency * capCDList[3], currentNoiseOffset) * noiseAmplitude * capCDList[3];
                    i++;
                }
                else
                {
                    cacheVertices[i].y = perlin.Noise(x * noiseFrequency * capCDList[4], y * noiseFrequency * capCDList[4], currentNoiseOffset) * noiseAmplitude * capCDList[4];
                    i++;
                }
			}
			
		}

		// Replace the old vertices with the new ones
		planeMesh.vertices = cacheVertices;

		// Generate new normals so that the lighting works correctly
		planeMesh.RecalculateNormals();

	}
}
