﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class API2 : MonoBehaviour
{
   Dictionary<string, string> serverAddr = new Dictionary<string, string>
    {
        { "sensor1", "https://io.adafruit.com/api/v2/Effervescence1/feeds/area1/data/last?X-AIO-Key=d3feb983d7f143d5864f81116930f2e6" },
        { "sensor2", "https://io.adafruit.com/api/v2/Effervescence1/feeds/area2/data/last?X-AIO-Key=d3feb983d7f143d5864f81116930f2e6" },
        { "sensor3", "https://io.adafruit.com/api/v2/Effervescence2/feeds/area3/data/last?X-AIO-Key=c54861cf59ae4d0fad91dc8c1c440b05" },
        { "sensor4", "https://io.adafruit.com/api/v2/Effervescence2/feeds/area4/data/last?X-AIO-Key=c54861cf59ae4d0fad91dc8c1c440b05" },
        { "sensor5", "https://io.adafruit.com/api/v2/Effervescence3/feeds/area5/data/last?X-AIO-Key=3c4e6b59b2e04f97aa8d3cfc3d3b182e" },
        { "sensor6", "https://io.adafruit.com/api/v2/Effervescence3/feeds/area6/data/last?X-AIO-Key=3c4e6b59b2e04f97aa8d3cfc3d3b182e" },
        { "sensor7", "https://io.adafruit.com/api/v2/Effervescence4/feeds/area7/data/last?X-AIO-Key=f4a490b3926c4d90be4873894ea35f22" },
        { "sensor8", "https://io.adafruit.com/api/v2/Effervescence4/feeds/area8/data/last?X-AIO-Key=f4a490b3926c4d90be4873894ea35f22" },
        { "sensor9", "https://io.adafruit.com/api/v2/Effervescence5/feeds/area9/data/last?X-AIO-Key=861ce1f0f92b430ca6fe646819087a71" },
        { "sensor10", "https://io.adafruit.com/api/v2/Effervescence5/feeds/area10/data/last?X-AIO-Key=861ce1f0f92b430ca6fe646819087a71" }
    };

    Dictionary<string, float> sensorValue = new Dictionary<string, float>
    {
        { "sensor1", 0.0f},
        { "sensor2", 0.0f},
        { "sensor3", 0.0f},
        { "sensor4", 0.0f},
        { "sensor5", 0.0f},
        { "sensor6", 0.0f},
        { "sensor7", 0.0f},
        { "sensor8", 0.0f},
        { "sensor9", 0.0f},
        { "sensor10", 0.0f}
    };

    public static Dictionary<string, float> cdBind = new Dictionary<string, float>
    {
        {"area1", 0.0f},
        {"area2", 0.0f},
        {"area3", 0.0f},
        {"area4", 0.0f},
        {"area5", 0.0f},
    };

    string value;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Getting data");

        StartCoroutine(GetRequest("sensor1"));
        StartCoroutine(GetRequest("sensor2"));
        StartCoroutine(GetRequest("sensor3"));
        StartCoroutine(GetRequest("sensor4"));
        StartCoroutine(GetRequest("sensor5"));
        StartCoroutine(GetRequest("sensor6"));
        StartCoroutine(GetRequest("sensor7"));
        StartCoroutine(GetRequest("sensor8"));
        StartCoroutine(GetRequest("sensor9"));
        StartCoroutine(GetRequest("sensor10"));
    }

    IEnumerator GetRequest(string key)
    {
        while (true)
        {
            string uri = serverAddr[key];
            using (UnityWebRequest wr = UnityWebRequest.Get(uri))
            {
                yield return wr.SendWebRequest();
                if (wr.isNetworkError)
                {
                    Debug.Log(wr.error);
                }
                else
                {
                    value = wr.downloadHandler.text;
                    var value1 = value.Split(',');
                    var value2 = value1[2].Split(':');
                    var found = value2[1].IndexOf('"');
                    var value3 = value2[1].Substring(found + 1, value2[1].Length - 2 - found);
                    var result = float.Parse(value3);

                    sensorValue[key] = result;
                    //Debug.Log(key);
                    //Debug.Log(result);

                }

                cdBind["area1"] = sensorValue["sensor1"] + sensorValue["sensor2"];
                if (cdBind["area1"] < 0) cdBind["area1"] = 0;
                cdBind["area2"] = sensorValue["sensor3"] - sensorValue["sensor4"];
                if (cdBind["area2"] < 0) cdBind["area2"] = 0;
                cdBind["area3"] = sensorValue["sensor6"] - sensorValue["sensor5"];
                if (cdBind["area3"] < 0) cdBind["area3"] = 0;
                cdBind["area4"] = sensorValue["sensor7"] - sensorValue["sensor8"];
                if (cdBind["area4"] < 0) cdBind["area4"] = 0;
                cdBind["area5"] = sensorValue["sensor9"] + sensorValue["sensor10"];
                if (cdBind["area5"] < 0) cdBind["area5"] = 0;
                //Debug.Log("area5");
                //Debug.Log(cdBind["area5"]);

                yield return new WaitForSeconds(5);
            }
        }
    }
}
