﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.Experimental.VFX;

public class Controller : MonoBehaviour
{

    // TODO: Put the CSV files in the Assets folder
    // CSV Data Variables

    // Non-Fiction
    private List<int> nonficGen = new List<int>();
    private List<float> nonficMood = new List<float>();

    // Fantasy
    private List<int> fantasyGen = new List<int>();
    private List<float> fantasyMood = new List<float>();

    // Romance
    private List<int> romanceGen = new List<int>();
    private List<float> romanceMood = new List<float>();

    // Science Fiction
    private List<int> scifiGen = new List<int>();
    private List<float> scifiMood = new List<float>();

    private Dictionary<string, List<int>> genDict = new Dictionary<string, List<int>>();         
    private Dictionary<string, List<float>> moodDict = new Dictionary<string, List<float>>();

    // Mode triggered
    public static int index;

    // Number of particles to spawn
    private float numParticles;         
    private float percentParticles;

    // Sentiment (mood) value
    public static float mood = 0.5f;          

    public bool nonFic;
    public bool fantasy;
    public bool romance;
    public bool sciFi;

    // TransitionTime is a multiplier for transition duration between modes
    // TransitionTime = 1.0 * 10 = 10 seconds
    private float transitionTime = 1.0f;
    
    // modeTime is a multiplier for duration that mode stays constant
    // modeTime = 2.0 * 10 = 20 seconds
    public float genreTime;
    private float moodTransitionTime = 5.0f;
    private float genTransitionInTime = 8.0f;
    private float genTransitionOutTime = 8.0f;

    // FOR SLIDER
    // totalModeTime = modeTime in seconds + transitionTime
    private float totalModeTime;
    private float elapsedTime;
    private bool modeChange;

    public AudioSource audioSource;
    public AudioClip[] Clips;

    public VisualEffect visualEffect;
    Dictionary<string, List<string>> spawnIDDict;
    Dictionary<string, List<int>> spawnNumDict;

    // Use this for initialization
    void Start()
    {
        List<string> spawnID_N = new List<string> { "spawn N1", "spawn N2" };
        List<string> spawnID_F = new List<string> { "spawn fantasy" };
        List<string> spawnID_R = new List<string> { "spawn R1", "spawn R2" };
        List<string> spawnID_S = new List<string> { "spawn S1", "spawn S2" };
        spawnIDDict = new Dictionary<string, List<string>>
        {
            {"mode0", spawnID_N},
            {"mode1", spawnID_F},
            {"mode2", spawnID_R},
            {"mode3", spawnID_S}
        };

        List<int> spawnNum_N = new List<int> { 50000, 30000 };
        List<int> spawnNum_F = new List<int> { 100000 };
        List<int> spawnNum_R = new List<int> { 2000, 200000 };
        List<int> spawnNum_S = new List<int> { 30000, 100000 };
        spawnNumDict = new Dictionary<string, List<int>>
        {
            {"mode0", spawnNum_N},
            {"mode1", spawnNum_F},
            {"mode2", spawnNum_R},
            {"mode3", spawnNum_S}
        };

        ReadCSVFile();
        StartCoroutine(GetValues());
    }

    //void Update()
    //{
    //    Timer();
    //}

    //void Timer()
    //{
    //    elapsedTime += Time.deltaTime;
    //    totalModeTime = genTransitionInTime + (moodTransitionTime + modeTime) * 10f + genTransitionOutTime + 0.5f;
    //    float minutes = elapsedTime / totalModeTime;
    //    string sliderTag = "slider" + index;
    //    var selectedBar = GameObject.FindGameObjectWithTag(sliderTag).GetComponent<Slider>();
    //    selectedBar.value = minutes;

    //    if (elapsedTime >= totalModeTime)
    //    {
    //        modeChange = true;
    //        elapsedTime = 0.0f;
    //        selectedBar.value = 0.0f;
    //    }

    //}


    IEnumerator GetValues()
    {
        float delta0 = -100.0f;
        float delta1 = -100.0f;
        float delta2 = -100.0f;
        float delta3 = -100.0f;
        List<float> deltaMode = new List<float> { delta0, delta1, delta2, delta3 };
        float maxDelta;
        List<int> numParticles_list = new List<int>();
        float genTransitionInRate;
        List<float> mood_list = new List<float>();
        float moodTransitionRate;
        float genTransitionOutRate;
        int spawnNum0 = 0;
        int spawnNum1 = 0;
        List<string> spawnID_list = new List<string>();
        List<int> spawnNum_list = new List<int>();
        int prevIndex = 0;
        List<string> spawnID_prev = new List<string>();


        string currentTag;
        Text selectedText;
        Color st;
        string prevTag;
        Text prevText;
        Color pt;

        string currentMoodTag;
        Text currentMoodText;
        Color cmt;
        string prevMoodTag;
        Text prevMoodText;
        Color pmt;


        for (int i = 1; i < nonficGen.Count; i++)
        {
            Debug.Log(Time.time);
            if (nonFic)
            {
                delta0 = nonficGen[i] - nonficGen[i - 1];
                deltaMode[0] = delta0;
            }
            if (fantasy)
            {
                delta1 = fantasyGen[i] - fantasyGen[i - 1];
                deltaMode[1] = delta1;
                // Soften volume for fantasy
                audioSource.volume = 0.7f;
            }

            if (romance)
            {
                delta2 = romanceGen[i] - romanceGen[i - 1];
                deltaMode[2] = delta2;
            }

            if (sciFi)
            {
                delta3 = scifiGen[i] - scifiGen[i - 1];
                deltaMode[3] = delta3;
            }

            maxDelta = deltaMode.Max();
            index = deltaMode.IndexOf(maxDelta);
            Debug.Log("######### MODE CHANGE ##########");
            Debug.Log("mode" + index);

            // Highlight the text of the current genre
            currentTag = "text" + index;
            selectedText = GameObject.FindGameObjectWithTag(currentTag).GetComponent<Text>();
            st = selectedText.color;
            st.a = 1.0f;
            selectedText.color = st;

            currentMoodTag = "mood" + index;
            currentMoodText = GameObject.FindGameObjectWithTag(currentMoodTag).GetComponent<Text>();
            cmt = currentMoodText.color;
            cmt.a = 1.0f;
            currentMoodText.color = cmt;


            spawnID_list = spawnIDDict["mode" + index];
            spawnNum_list = spawnNumDict["mode" + index];

            audioSource.clip = Clips[index];
            audioSource.Play();

            // Interval between each transition
            genTransitionInRate = 0.0f;
            while (genTransitionInRate < genTransitionInTime)
            {
                audioSource.volume = Mathf.Lerp(audioSource.volume, 1.0f, Time.deltaTime * genTransitionInRate);
                if (index == 1)
                {
                    spawnNum0 = (int)Mathf.Lerp(spawnNum0, spawnNum_list[0], Time.deltaTime * genTransitionInRate);
                    visualEffect.SetInt(spawnID_list[0], spawnNum0);
                }
                else
                {
                    spawnNum0 = (int)Mathf.Lerp(spawnNum0, spawnNum_list[0], Time.deltaTime * genTransitionInRate);
                    spawnNum1 = (int)Mathf.Lerp(spawnNum1, spawnNum_list[1], Time.deltaTime * genTransitionInRate);
                    visualEffect.SetInt(spawnID_list[0], spawnNum0);
                    visualEffect.SetInt(spawnID_list[1], spawnNum1);
                }
                genTransitionInRate += Time.deltaTime / transitionTime;
                yield return null;
            }

            spawnID_prev = spawnIDDict["mode" + prevIndex];
            if (index != prevIndex)
            {
                if (prevIndex == 1)
                {
                    visualEffect.SetInt(spawnID_prev[0], 0);
                }
                else
                {
                    visualEffect.SetInt(spawnID_prev[0], 0);
                    visualEffect.SetInt(spawnID_prev[1], 0);
                }
            }
     
            numParticles_list = genDict["mode" + index];
            mood_list = moodDict["mode" + index];

            for (int j = i; j < i + 10; j++)
            {
                //percentParticles = numParticles_list[j] / 100.0f;

                //switch (index)
                //{
                //    case 0:
                //        numParticles = percentParticles * 2000 + 3000;
                //        Debug.Log("#Particles=" + numParticles);
                //        break;
                //    case 1:
                //        numParticles = 0;
                //        Debug.Log("#Particles=" + numParticles);
                //        break;
                //    case 2:
                //        numParticles = 0;
                //        Debug.Log("#Particles=" + numParticles);
                //        break;
                //    case 3:
                //        numParticles = 0;
                //        Debug.Log("#Particles=" + numParticles);
                //        break;
                //}

                moodTransitionRate = 0.0f;
                while (moodTransitionRate < moodTransitionTime)
                {
                    mood = Mathf.Lerp(mood, mood_list[j], Time.deltaTime * moodTransitionRate);
                    currentMoodText.text = mood.ToString();
                    moodTransitionRate += Time.deltaTime / transitionTime;
                    //Debug.Log(mood);
                    yield return null;
                }
                // Each minute - set to 60f
                yield return new WaitForSeconds(genreTime);
            }
            prevIndex = index;
            genTransitionOutRate = 0.0f;
            while (genTransitionOutRate < genTransitionOutTime)
            {
                audioSource.volume = Mathf.Lerp(audioSource.volume, 0.0f, Time.deltaTime * genTransitionOutRate);

                if (index == 1)
                {
                    spawnNum0 = (int)Mathf.Lerp(spawnNum0, 8000, Time.deltaTime * genTransitionOutRate);
                    visualEffect.SetInt(spawnID_list[0], spawnNum0);
                }
                else
                {
                    spawnNum0 = (int)Mathf.Lerp(spawnNum0, 300, Time.deltaTime * genTransitionOutRate);
                    spawnNum1 = (int)Mathf.Lerp(spawnNum1, 3000, Time.deltaTime * genTransitionOutRate);
                    visualEffect.SetInt(spawnID_list[0], spawnNum0);
                    visualEffect.SetInt(spawnID_list[1], spawnNum1);
                }

                genTransitionOutRate += Time.deltaTime / transitionTime;
                yield return null;
            }

            // Unhighlight the text of the previous genre
            prevTag = "text" + index;
            prevText = GameObject.FindGameObjectWithTag(prevTag).GetComponent<Text>();
            pt = prevText.color;
            pt.a = 0.47f;
            prevText.color = pt;

            prevMoodTag = "mood" + index;
            prevMoodText = GameObject.FindGameObjectWithTag(prevMoodTag).GetComponent<Text>();
            pmt = prevMoodText.color;
            pmt.a = 0.47f;
            prevMoodText.color = pmt;

            prevMoodText.text = "";

            if (i == nonficGen.Count - 10)
            {
                i = 1;
            }
        }
    }

    // Load files
    public IEnumerable<string> ReadAllLines(string filePath, Encoding encoding)
    {
        StreamReader streamReader = new StreamReader(filePath, encoding);
        bool endFile = false;
        string line;
        using (streamReader)
        {
            while (!endFile)
            {
                line = streamReader.ReadLine();
                if (line == null)
                {
                    endFile = true;
                    break;
                }
                yield return line;
            }
        }
    }

    // Update is called once per frame
    void ReadCSVFile()
    {
        // Load files
        string timeFile = Application.dataPath + "/BigData.csv";
        Encoding encoding = Encoding.GetEncoding(1250);

        string[] lineValues;
        // Search Interest Over Time
        foreach (var i in ReadAllLines(timeFile, encoding).Skip(1))
        {
            lineValues = i.Split(',');

            // Processing Genre values
            nonficGen.Add(int.Parse(lineValues[1]));
            fantasyGen.Add(int.Parse(lineValues[2]));
            romanceGen.Add(int.Parse(lineValues[3]));
            scifiGen.Add(int.Parse(lineValues[4]));
            nonficMood.Add(float.Parse(lineValues[5]));
            fantasyMood.Add(float.Parse(lineValues[6]));
            romanceMood.Add(float.Parse(lineValues[7]));
            scifiMood.Add(float.Parse(lineValues[8]));
        }
        genDict.Add("mode0", nonficGen);
        genDict.Add("mode1", fantasyGen);
        genDict.Add("mode2", romanceGen);
        genDict.Add("mode3", scifiGen);

        moodDict.Add("mode0", nonficMood);
        moodDict.Add("mode1", fantasyMood);
        moodDict.Add("mode2", romanceMood);
        moodDict.Add("mode3", scifiMood);
    }
}