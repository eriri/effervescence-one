﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Joint = Windows.Kinect.Joint;

public class BodySourceView : MonoBehaviour 
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;

    public AudioSource audioSource;
    public AudioClip clip;

    public GameObject LH1;
    public GameObject RH1;
    public GameObject LH2;
    public GameObject RH2;
    public GameObject LH3;
    public GameObject RH3;
    public GameObject LH4;
    public GameObject RH4;
    public GameObject LH5;
    public GameObject RH5;
    public GameObject LH6;
    public GameObject RH6;
    public Vector3 LeftTemp;
    public Vector3 RightTemp;
    public Vector3 Temp;

    private Dictionary<ulong, int> Handsets = new Dictionary<ulong, int>();
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };
    //private List<JointType> _joints = new List<JointType>
    //{
    //    JointType.HandLeft,
    //    JointType.HandRight,
    //};

    private Dictionary<int, int> AvailableHandsets = new Dictionary<int, int>()
    {
        {1,0},
        {2,0},
        {3,0},
        {4,0},
        {5,0},
        {6,0},
    };

    void Start()
    {

        LH1 = GameObject.Find("LH1");
        RH1 = GameObject.Find("RH1");
        LH2 = GameObject.Find("LH2");
        RH2 = GameObject.Find("RH2");
        LH3 = GameObject.Find("LH3");
        RH3 = GameObject.Find("RH3");
        LH4 = GameObject.Find("LH4");
        RH4 = GameObject.Find("RH4");
        LH5 = GameObject.Find("LH5");
        RH5 = GameObject.Find("RH5");
        LH6 = GameObject.Find("LH6");
        RH6 = GameObject.Find("RH6");

        //Set default postions
        LH1.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH1.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        LH2.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH2.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        LH3.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH3.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        LH4.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH4.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        LH5.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH5.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        LH6.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        RH6.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
        //for (int i = 0; i< Handsets.Count; i++)
        //{
        //    print(Handsets[i]);
        //}


    }
    void Update () 
    {
        if (BodySourceManager == null)
        {
            return;
        }
        
        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }
        
        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }
        
        List<ulong> trackedIds = new List<ulong>();
        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
              }
                
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
                //print(body.TrackingId);
            }
        }
        
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
                AvailableHandsets.Add(Handsets[trackingId], 0);
                
                // Reset to default position
                if (Handsets[trackingId] == 1)
                {
                    LH1.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH1.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    
                }
                if (Handsets[trackingId] == 2)
                {
                    LH2.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH2.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    
                }
                if (Handsets[trackingId] == 3)
                {
                    LH3.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH3.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    
                }
                if (Handsets[trackingId] == 4)
                {
                    LH4.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH4.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                   
                }
                if (Handsets[trackingId] == 5)
                {
                    LH5.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH5.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    
                }
                if (Handsets[trackingId] == 6)
                {
                    LH6.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    RH6.transform.position = new Vector3(100.0f, 0.0f, 0.0f);
                    
                }
                Handsets.Remove(trackingId);
            }
        }

        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
            }
            
            if(body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                    for(int i = 1; i < 7; i++)
                    {
                        
                        if (AvailableHandsets.ContainsKey(i))
                        {
                            Handsets[body.TrackingId] = i;
                            print(Handsets[body.TrackingId]);
                            AvailableHandsets.Remove(i);
                            break;
                        }
                    }
                    
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId], Handsets[body.TrackingId]);
            }
        }

    }

    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        // Play sound
        audioSource.clip = clip;
        audioSource.Play();

        return body;
    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject, int ID)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;
            
            if(_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }
            if (jt == Kinect.JointType.WristLeft)
            {
                LeftTemp = GetVector3FromJoint(sourceJoint);
                LeftTemp.z = 0.0f;

                //Move the hands
                if (ID == 1)
                {
                    LH1.transform.position = LeftTemp;
                    
                }
                if (ID == 2)
                {
                    LH2.transform.position = LeftTemp;
                    
                }
                if (ID == 3)
                {
                    LH3.transform.position = LeftTemp;
                    
                }
                if (ID == 4)
                {
                    LH4.transform.position = LeftTemp;
                    
                }
                if (ID == 5)
                {
                    LH5.transform.position = LeftTemp;
                   
                }
                if (ID == 6)
                {
                    LH6.transform.position = LeftTemp;
                    
                }
            }
            if (jt == Kinect.JointType.WristRight)
            {
                RightTemp = GetVector3FromJoint(sourceJoint);
                RightTemp.z = 0.0f;
                if (ID == 1)
                {
                    RH1.transform.position = RightTemp;
                    
                }
                if (ID == 2)
                {
                    RH2.transform.position = RightTemp;
                   
                }
                if (ID == 3)
                {
                    RH3.transform.position = RightTemp;
                    
                }
                if (ID == 4)
                {
                    RH4.transform.position = RightTemp;
                    
                }
                if (ID == 5)
                {
                    RH5.transform.position = RightTemp;
                    
                }
                if (ID == 6)
                {
                    RH6.transform.position = RightTemp;
                    
                }
            }
            Transform jointObj = bodyObject.transform.Find(jt.ToString());
        
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }
    
    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 2.0f, joint.Position.Y * 1f, joint.Position.Z * 0.8f);
    }
}
